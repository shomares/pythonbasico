import figura
class Rectangulo(figura.Figura):
    def __init__(self, width : float, heigth: float):
        self.width = width
        self.heigth = heigth
    
    def getArea(self) -> float:
        return self.width * self.heigth
    
    def getPerimetro(self) ->float:
        return self.width * 2 + self.heigth * 2