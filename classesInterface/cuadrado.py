import figura

class Cuadrado(figura.Figura):
    def __init__(self, lado : float):
        self.lado = lado

    def getArea(self)->float:
        return self.lado * self.lado
    
    def getPerimetro(self)->float:
        return self.lado * 2