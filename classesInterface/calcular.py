import figura

class Calcular:
    def calcular(self, figura : figura.Figura)-> map:
        return {
            "perimetro" : figura.getPerimetro(),
            "area" : figura.getArea()
        }