from abc import ABCMeta, abstractmethod
class Figura:
    @abstractmethod
    def getArea(self)->float:
        raise NotImplementedError
    
    @abstractmethod
    def getPerimetro(self)->float:
        raise NotImplementedError
