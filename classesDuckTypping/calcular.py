import calculorecursivo

class Calcular:
    def calcular(self, n: int, elemento : calculorecursivo.CalculoRecursivo)->map:
        return {
            "resultado" : elemento.calcular(n),
            "name" : elemento.getName()
        }