class Fibonnacci:
    def calcular(self, n: int) ->int:
        if (n>1):
            return self.calcular(n-1) + self.calcular(n-2)
        elif n == 1:
            return 1
        else:
            return 0
   
    def getName(self)->str:
        return "Fibonnaci"