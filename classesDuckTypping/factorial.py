class Factorial:
    def calcular(self, n: int)-> int:
        if (n==0):
            return 1
        else:
            return n * self.calcular(n-1)
    def getName(self)->str:
        return "Factorial"