class Palindromo:
    def __init__(self, cadena : str):
        self.cadena = cadena
    

    def isPalindromo(self)->bool:
        size = len(self.cadena)
        z = -1
        for i in range(size):
            if (self.cadena[i] != self.cadena[z]):
                return False
            z-= 1
        
        return True
        

