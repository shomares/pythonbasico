import palindromo

cadena = "ana"

calcular = palindromo.Palindromo("ana")
print (calcular.cadena)
print (calcular.isPalindromo())

calcular = palindromo.Palindromo("nosoypalindromo")
print(calcular.cadena)
print(calcular.isPalindromo())

calcular = palindromo.Palindromo("anitalavalatina")
print(calcular.cadena)
print(calcular.isPalindromo())
