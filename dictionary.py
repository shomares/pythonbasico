import random

calificaciones = {'matematicas' : [random.randint(8, 10) for i in range(5)],
                  'espanol' : [random.randint(0,10) for i in range(5)],
                  'ciencias': [random.randint(0,10) for i in range(5)]}

print (calificaciones)

result = {}

for key,value in calificaciones.items():
    total = 0
    average = 0
    for val in value:
        total += val
    average = total /  len(value)
    result[key] = average

print(result)

total = 0
for key,value in result.items():
    total += value

total = total / len(result)

print ("Promedio Final {}".format(total) )
