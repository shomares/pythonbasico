import position
import pygame 
class Snakehead:
    def __init__(self, position : position.Position, width:int, heigth:int):
        self.position = position
        self.width = width
        self.heigth = heigth
        self.lastMove = None
        self.speed = 16

    def center(self, width : int, heigth : int):
        self.position = position.Position(width/2 - self.width, heigth/2 - self.heigth)

    def read(self, key):
        if (key == pygame.K_LEFT and self.lastMove != 'rigth'):
            self.lastMove = 'left' 
        elif (key == pygame.K_DOWN and self.lastMove != 'up'):
            self.lastMove = 'down'
        elif (key == pygame.K_UP and self.lastMove != 'down'):
            self.lastMove = 'up'
        elif(key == pygame.K_RIGHT and self.lastMove != 'left') :
            self.lastMove = 'rigth'

    def move(self):
       positionS = self.position
       if (self.lastMove != None):
            if (self.lastMove == 'left'):
                self.position = position.Position(positionS.x - self.speed, positionS.y)
            elif (self.lastMove == 'rigth'):
                self.position = position.Position(positionS.x + self.speed, positionS.y)
            elif (self.lastMove == 'up'):
                self.position = position.Position(positionS.x, positionS.y - self.speed )
            elif (self.lastMove == 'down'):
                self.position = position.Position(positionS.x, positionS.y + self.speed )
     
    def getPosition(self):
        return self.position.x, self.position.y
