import pygame
import snakehead
import position
import food
import tail

class Tablero:
    def __init__(self, ScreenW : int, ScreenH:int):
        pygame.init()
        self.ScreenW = ScreenW
        self.ScreenH = ScreenH
        self.display = pygame.display.set_mode([ScreenW, ScreenH])
        self.display.fill([255, 255, 255])
        self.gameOver = False
        self.snakehead = snakehead.Snakehead(position.Position(0,0), 16, 16)
        self.snakehead.center(self.ScreenW, self.ScreenH)
        self.food = food.Food(16, 16)
        self.food.respawn(self.ScreenW, self.ScreenH)
        self.score = 0
        self.tails = []
        self.font = pygame.font.SysFont(None, 30)
    
    def restart(self):
        self.snakehead.center(self.ScreenW, self.ScreenH)
        self.tails.clear()
        self.score = 0

    
    def drawFood(self):
        pos = self.food.position
        pygame.draw.rect(self.display, [self.food.color,0,0], [pos.x, pos.y, self.food.width, self.food.heigth])


    def drawSnakeHead(self):
        self.snakehead.move()
        pos = self.snakehead.position
        heigth = self.snakehead.heigth
      
        if pos.x < 0 or pos.x > self.ScreenW or pos.y < 0 or pos.y > self.ScreenH - heigth/2:
            self.restart()
            
        position = self.snakehead.position
        pygame.draw.rect(self.display, [0,0,0], [position.x, position.y, self.snakehead.width, self.snakehead.heigth])
     
   
     
    def isCollision(self):
         positionXs, positionYs = self.snakehead.getPosition()
         positionXf , positionYf = self.food.getPosition()

       
         if positionXs + self.snakehead.width > positionXf and positionXs < positionXf + self.food.width:
             if positionYs + self.snakehead.heigth> positionYf and positionYs < positionYf + self.food.heigth:
                 self.food.respawn(self.ScreenW, self.ScreenH)
                 self.tails.append(tail.Tail(position.Position(positionXs, positionYf), self.ScreenW, self.ScreenH))
                 self.score += 1

    def isCollisionItself(self):
        headerX, headerY = self.snakehead.getPosition()
        if (len(self.tails)> 2):
            try:
                for i in range(len(self.tails)-1):
                    tail = self.tails[i]
                    if i > 1:
                        positionX, positionY = tail.getPosition()
                        if positionX == headerX and positionY == headerY:
                            self.restart()
            except:
                self.restart()
        
    def drawTails(self):
        headerX, headerY = self.snakehead.getPosition()

        if (len(self.tails) > 0):
            size = len(self.tails) -1

            for i in range(len(self.tails)-1):
                    tailX, tailY = self.tails[size - i - 1 ].getPosition()
                    tailAux = self.tails[size -i ]
                    tailAux.position = position.Position(tailX, tailY)
            
            self.tails[0].position = position.Position(headerX, headerY)
          
           
            for tailAux in self.tails:
                tailX, tailY = tailAux.getPosition()
                pygame.draw.rect(self.display, tailAux.color, [tailX, tailY, self.snakehead.width, self.snakehead.heigth])
    
    def drawScore (self):
        text = "Marcador {}".format(self.score)
        label = self.font.render(text, True, [0,0,0])
        self.display.blit(label, (100,50))

    def draw(self):
        Fps = 8
        timer_clock = pygame.time.Clock()

        while not self.gameOver:
            for event in pygame.event.get():
                if event.type ==  pygame.QUIT:
                    self.gameOver = True
                    break
                elif event.type == pygame.KEYDOWN:
                    self.snakehead.read(event.key)
            self.display.fill([255, 255, 255])
            self.isCollision()
            self.isCollisionItself()
            self.drawFood()
            self.drawTails()
            self.drawSnakeHead()
            self.drawScore()
           
            pygame.display.flip()
            pygame.display.update()
            timer_clock.tick(Fps)
