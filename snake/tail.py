import position
import random
class Tail:
    def __init__(self, position:position.Position, width: float, heigth: float):
        self.position = position
        self.width = width
        self.heigth = heigth
        self.color = [random.randint(0, 255) for i in range(3)]
    
    def getPosition(self):
        return self.position.x, self.position.y